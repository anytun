/*
 *  anytun
 *
 *  The secure anycast tunneling protocol (satp) defines a protocol used
 *  for communication between any combination of unicast and anycast
 *  tunnel endpoints.  It has less protocol overhead than IPSec in Tunnel
 *  mode and allows tunneling of every ETHER TYPE protocol (e.g.
 *  ethernet, ip, arp ...). satp directly includes cryptography and
 *  message authentication based on the methodes used by SRTP.  It is
 *  intended to deliver a generic, scaleable and secure solution for
 *  tunneling and relaying of packets of any protocol.
 *
 *
 *  Copyright (C) 2007-2008 Othmar Gsenger, Erwin Nindl, 
 *                          Christian Pointner <satp@wirdorange.org>
 *
 *  This file is part of Anytun.
 *
 *  Anytun is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 3 as
 *  published by the Free Software Foundation.
 *
 *  Anytun is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with anytun.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CRYPTINIT_HPP
#define _CRYPTINIT_HPP

#ifndef NOCRYPT
#ifndef USE_SSL_CRYPTO
#include <gcrypt.h>

// boost thread callbacks for libgcrypt
#if defined(BOOST_HAS_PTHREADS)

static int boost_mutex_init(void **priv)
{
  boost::mutex *lock = new boost::mutex();
  if (!lock)
    return ENOMEM;
  *priv = lock;
  return 0;
}

static int boost_mutex_destroy(void **lock)
{
  delete reinterpret_cast<boost::mutex*>(*lock);
  return 0;
}

static int boost_mutex_lock(void **lock)
{
  reinterpret_cast<boost::mutex*>(*lock)->lock();
  return 0;
}

static int boost_mutex_unlock(void **lock)
{
  reinterpret_cast<boost::mutex*>(*lock)->unlock();
  return 0;
}

static struct gcry_thread_cbs gcry_threads_boost =
{ GCRY_THREAD_OPTION_USER, NULL,
  boost_mutex_init, boost_mutex_destroy,
  boost_mutex_lock, boost_mutex_unlock };
#else
#error this libgcrypt thread callbacks only work with pthreads
#endif


#define MIN_GCRYPT_VERSION "1.2.0"

bool initLibGCrypt()
{
  // make libgcrypt thread safe 
  // this must be called before any other libgcrypt call
  gcry_control( GCRYCTL_SET_THREAD_CBS, &gcry_threads_boost );

  // this must be called right after the GCRYCTL_SET_THREAD_CBS command
  // no other function must be called till now
  if( !gcry_check_version( MIN_GCRYPT_VERSION ) ) {
    std::cout << "initLibGCrypt: Invalid Version of libgcrypt, should be >= " << MIN_GCRYPT_VERSION << std::endl;
    return false;
  }

  gcry_error_t err = gcry_control (GCRYCTL_DISABLE_SECMEM, 0);
  if( err ) {
    std::cout << "initLibGCrypt: Failed to disable secure memory: " << LogGpgError(err) << std::endl;
    return false;
  }

  // Tell Libgcrypt that initialization has completed.
  err = gcry_control(GCRYCTL_INITIALIZATION_FINISHED);
  if( err ) {
    std::cout << "initLibGCrypt: Failed to finish initialization: " << LogGpgError(err) << std::endl;
    return false;
  }

  cLog.msg(Log::PRIO_NOTICE) << "initLibGCrypt: libgcrypt init finished";
  return true;
}
#endif
#endif

#endif

